package com.w2a.testcases;

import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Test;

import com.w2a.base.TestBase;
import com.w2a.utilities.TestUtil;

public class AddCustomerTest extends TestBase {
	
	
	@Test(dataProviderClass=TestUtil.class,dataProvider = "dp")
	public void addCustomerTest(String firstname, String lastname, String postcode,String alertText,String runmode) throws InterruptedException {
		if(!runmode.equals("Y")) {
			
			throw new SkipException("Skipping the test as the Runnable modeRunnable isElementPresent(NO)");
		}
		
		click("addCustBtn_CSS");
		type("firstName_XPATH",firstname);
		type("lastName_XPATH",lastname);
		type("postCode_XPATH",postcode);
		click("addBtn_XPATH");
				
		Alert alert=wait.until(ExpectedConditions.alertIsPresent());
		Assert.assertTrue(alert.getText().contains(alertText));
		alert.accept();
		
	}
}