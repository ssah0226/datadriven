package com.w2a.testcases;

import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import com.w2a.base.TestBase;
import com.w2a.utilities.TestUtil;

public class OpenAccountTest extends TestBase {

	@Test(dataProviderClass=TestUtil.class,dataProvider = "dp")
	public void openAccountTest(String customer, String currency) {

		click("openAccBtn_XPATH");
		select("customer_XPATH", customer);
		select("curreny_XPATH", currency);
		click("processBtn_XPATH");

		Alert alert = wait.until(ExpectedConditions.alertIsPresent());
		alert.accept();
	}

}